package main

import (
	"bytes"
	"fmt"
	"reflect"
	"testing"
)

/*
go test
go test -v -run TestSlicesMergeEmpty
go test -cover

https://golang.org/pkg/testing/
t.Parallel()
t.Skip("Skip until ready for testing merge sort")
*/

var nilslice []int

var empty = []int{}

// https://blog.golang.org/subtests
func TestParameterized(t *testing.T) {
	// testCases :=
}

// defining the test structure separately and clear naming helps readability
type slicesMergeTest struct {
	a        []int
	b        []int
	expected []int
}

func TestSlicesMergeEmptyAndBasic(t *testing.T) {
	testCases := []slicesMergeTest{
		{a: empty, b: empty, expected: empty},
		{a: empty, b: nil, expected: empty},
		{a: nil, b: empty, expected: empty},
		{a: nil, b: nil, expected: empty},
		{a: []int{1}, b: nil, expected: []int{1}},
		{a: nil, b: []int{1}, expected: []int{1}},
		{a: []int{1, 2}, b: nil, expected: []int{1, 2}},
		{a: nil, b: []int{1, 2}, expected: []int{1, 2}},
		{a: []int{2, 4, 6}, b: []int{1, 3, 7, 9}, expected: []int{1, 2, 3, 4, 6, 7, 9}},
		{a: []int{1, 3}, b: []int{-2870837225030527764, -2}, expected: []int{-2870837225030527764, -2, 1, 3}},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%#v merged with %#v", tc.a, tc.b), func(t *testing.T) {
			actual := SlicesMerge(tc.a, tc.b)
			assertSlicesEqual(t, tc.expected, actual)
		})
	}
}

func TestSlicesMergeAdvanced(t *testing.T) {
	testCases := []slicesMergeTest{
		{a: []int{9}, b: []int{2, 4, 6}, expected: []int{2, 4, 6, 9}},
		{a: []int{2, 4, 6}, b: []int{9}, expected: []int{2, 4, 6, 9}},
		{a: []int{1}, b: []int{2, 4, 6}, expected: []int{1, 2, 4, 6}},
		{a: []int{2, 4, 6}, b: []int{1}, expected: []int{1, 2, 4, 6}},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%#v merged with %#v", tc.a, tc.b), func(t *testing.T) {
			actual := SlicesMerge(tc.a, tc.b)
			assertSlicesEqual(t, tc.expected, actual)
		})
	}
}

func TestSliceSplitEmptyZero(t *testing.T) {
	result, err := SliceSplit(empty, 0)
	expected := "Cannot split length 0 into 0 pieces"
	if err.Error() != expected {
		t.Error("\nExpected: ", expected, "\nReceived: ", err, result)
	}

	result, err = SliceSplit(empty, 2)
	expected = "Cannot split length 0 into 2 pieces"
	if err.Error() != expected {
		t.Error("\nExpected: ", expected, "\nReceived: ", err, result)
	}

	result, err = SliceSplit([]int{1}, 0)
	expected = "Cannot split length 1 into 0 pieces"
	if err.Error() != expected {
		t.Error("\nExpected: ", expected, "\nReceived: ", err, result)
	}
}

func TestSliceSplitToOnes(t *testing.T) {
	result, err := SliceSplit([]int{1}, 1)
	assertErrorNil(t, err)
	assertSlicesEqual(t, []int{1}, result[0])

	result, err = SliceSplit([]int{0, 2}, 2)
	assertErrorNil(t, err)
	assertSlicesEqual(t, []int{0}, result[0])
	assertSlicesEqual(t, []int{2}, result[1])

	result, err = SliceSplit([]int{-1, -2, -3}, 3)
	assertErrorNil(t, err)
	assertSlicesEqual(t, []int{-1}, result[0])
	assertSlicesEqual(t, []int{-2}, result[1])
	assertSlicesEqual(t, []int{-3}, result[2])

	//TODO: parameterized test for loop to some high range
}

func TestSliceSplitEvenToTwos(t *testing.T) {
	result, err := SliceSplit([]int{1, 2, 3, 4}, 2)
	assertErrorNil(t, err)
	assertSlicesEqual(t, []int{1, 2}, result[0])
	assertSlicesEqual(t, []int{3, 4}, result[1])

	result, err = SliceSplit([]int{-100, -99, 1, 2, 3, 4}, 3)
	assertErrorNil(t, err)
	assertSlicesEqual(t, []int{-100, -99}, result[0])
	assertSlicesEqual(t, []int{1, 2}, result[1])
	assertSlicesEqual(t, []int{3, 4}, result[2])

	//TODO: parameterized test for loop to some high range
}

func TestSliceSplitThreeByTwo(t *testing.T) {
	result, err := SliceSplit([]int{1, 2, 3}, 2)
	assertErrorNil(t, err)
	assertSlicesEqual(t, []int{1}, result[0])
	assertSlicesEqual(t, []int{2, 3}, result[1])
}

func TestSliceSplitFiveByTwo(t *testing.T) {
	result, err := SliceSplit([]int{1, 2, 3, 4, 5}, 2)
	assertErrorNil(t, err)
	assertSlicesEqual(t, []int{1, 2}, result[0])
	assertSlicesEqual(t, []int{3, 4, 5}, result[1])
}

func TestSliceSplitThreeByThree(t *testing.T) {
	n := []int{99, 66, 33, -99, -66, -33, -1, 2, 3}
	result, err := SliceSplit(n, 3)
	assertErrorNil(t, err)
	assertSlicesEqual(t, []int{99, 66, 33}, result[0])
	assertSlicesEqual(t, []int{-99, -66, -33}, result[1])
	assertSlicesEqual(t, []int{-1, 2, 3}, result[2])
}

func TestSliceSplitTenByThree(t *testing.T) {
	n := []int{99, 66, 33, -99, -66, -33, 0, -1, 2, 3}
	result, err := SliceSplit(n, 3)
	assertErrorNil(t, err)
	assertSlicesEqual(t, []int{99, 66, 33}, result[0])
	assertSlicesEqual(t, []int{-99, -66, -33}, result[1])
	assertSlicesEqual(t, []int{0, -1, 2, 3}, result[2])
}

func TestMergeSortEmpty(t *testing.T) {
	assertSlicesEqual(t, []int{}, MergeSort(empty))
	assertSlicesEqual(t, nil, MergeSort(nil))
}

// TODO: parameterized testing
func TestMergeSortReverse(t *testing.T) {
	n := []int{3, 2, 1, 0}
	result := MergeSort(n)
	assertSlicesEqual(t, []int{0, 1, 2, 3}, result)
}

func TestMergeSortSimple(t *testing.T) {
	assertSlicesEqual(t, []int{-1, 1, 2, 3, 7, 9}, MergeSort([]int{2, -1, 1, 3, 7, 9}))
}

// Helper Functions to reduce assertion code
func assertErrorNil(t *testing.T, err error) {
	if err != nil {
		t.Error("\nNo error expected, instead Received: ", err)
	}
}

func assertSlicesEqual(t *testing.T, expected []int, result []int) {
	if !reflect.DeepEqual(expected, result) {
		t.Error("\nExpected:", expected, "\nReceived: ", result)
	}
}

// BENCHMARKS

func MyConcatSimple(a string, b string) string {
	return a + b
}

func MyConcatSimpleLooped(a string, b string) string {
	for i := 0; i < 101; i++ {
		a += b
	}
	return a
}

func MyConcatBytesBuffer(a string, b string) string {
	var buffer bytes.Buffer
	buffer.WriteString(a)
	buffer.WriteString(b)
	return buffer.String()
}

func MyConcatBytesBufferLooped(a string, b string) string {
	var buffer bytes.Buffer
	buffer.WriteString(a)
	for i := 0; i < 101; i++ {
		buffer.WriteString(b)
	}
	return buffer.String()
}

func BenchmarkConcatSimple(b *testing.B) {
	for n := 0; n < b.N; n++ {
		MyConcatSimple("foo", "bar")
	}
}

func BenchmarkConcatSimpleLooped(b *testing.B) {
	for n := 0; n < b.N; n++ {
		MyConcatSimpleLooped("foo", "bar")
	}
}

func BenchmarkConcatBytesBuffer(b *testing.B) {
	for n := 0; n < b.N; n++ {
		MyConcatBytesBuffer("foo", "bar")
	}
}

func BenchmarkConcatBytesBufferLooped(b *testing.B) {
	for n := 0; n < b.N; n++ {
		MyConcatBytesBufferLooped("foo", "bar")
	}
}

// go test -bench=.
/*
BenchmarkStringConcat-4         30000000                40.3 ns/op
PASS
ok      bitbucket.org/johnpfeiffer/go-slice-mergesort   1.253s
*/
