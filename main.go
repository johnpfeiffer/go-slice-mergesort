package main

import (
	"fmt"
)

// SliceSplit is a function to split a slice into roughly even partitions
// https://golang.org/doc/effective_go.html#two_dimensional_slices
/* A more specific implementation than SliceSplit with the special case partitionSize = 1 could have been
result := make([][]int, len(n), len(n))
for i := 0; i < len(n); i++ {
    element := make([]int, 1, 1)
    element[0] = n[i]
    result[i] = element
}
return result
*/
func SliceSplit(n []int, count int) ([][]int, error) {
	result := [][]int{}
	// TODO: is there a better way of handling split into 0 pieces?
	if count == 0 || count > len(n) {
		return result, fmt.Errorf("Cannot split length %d into %d pieces", len(n), count)
	}

	partitionSize := len(n) / count
	for i, k := 0, 0; i < count; i, k = i+1, k+partitionSize {
		a := n[k : k+partitionSize]
		// special case to pad the last partition with all elements
		if i == count-1 {
			a = n[k:]
		}
		result = append(result, a)
	}
	return result, nil
}

// SlicesMerge takes two sorted slices of integers and merges them into a single sorted slice of integers
func SlicesMerge(a []int, b []int) []int {
	s := make([]int, len(a)+len(b))
	for ai, bi, si := len(a)-1, len(b)-1, len(a)+len(b)-1; si >= 0; si-- {
		if ai < 0 {
			s[si] = b[bi]
			bi--
		} else if bi < 0 {
			s[si] = a[ai]
			ai--
		} else if a[ai] > b[bi] {
			s[si] = a[ai]
			ai--
		} else {
			s[si] = b[bi]
			bi--
		}
	}
	return s
}

//MergeConsecutiveElements joins two consecutive slice elements together
func MergeConsecutiveElements(a [][]int) [][]int {
	var result [][]int
	for i, k := 0, 0; i < len(a); i++ {
		if i+1 < len(a) {
			result = append(result, SlicesMerge(a[i], a[i+1]))
			k++
			i++
		} else {
			result = append(result, a[i])
		}
	}
	return result
}

// MergeSort uses the merge sort algorithm to return a sorted a slice of integers
func MergeSort(n []int) []int {
	if len(n) <= 1 {
		return n
	}
	parts, _ := SliceSplit(n, len(n))
	result := MergeConsecutiveElements(parts)
	for len(result) > 1 {
		result = MergeConsecutiveElements(result)
	}

	return result[0]
}

func main() {

	n := []int{2, 1, 0, -1}
	fmt.Println("unsorted start:", n)
	fmt.Println("sorted:", MergeSort(n))

	n = []int{0, 1, 2}
	fmt.Println("unsorted start:", n)
	fmt.Println("sorted:", MergeSort(n))

	n = []int{9, 2, 4, 3}
	fmt.Println("unsorted start:", n)
	fmt.Println("sorted:", MergeSort(n))

	n = []int{9, 8, 7, 3, 2, 5}
	fmt.Println("unsorted start:", n)
	fmt.Println("sorted:", MergeSort(n))

	fmt.Println("done")
}
