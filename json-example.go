package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

func logIfError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// readFile is a convenience function to read a whole file at once
func readFile(f *os.File) {
	var data = make([]byte, 1024)
	totalBytes := 0
	for {
		count, err := f.Read(data)
		// https://golang.org/pkg/io/ , EOF is an expected error condition
		if err != io.EOF {
			logIfError(err)
		}
		// TODO: 0 bytes could be returned when not an EOF
		if count == 0 {
			break
		}
		totalBytes += count
		fmt.Printf("Read %d bytes: \n%s\n", count, string(data))
	}
	fmt.Printf("Read %d total bytes from the file\n", totalBytes)
}

// genericParsing is an example of the empty interface https://blog.golang.org/json-and-go
// https://en.wikipedia.org/wiki/JSON
func genericParsing(data []byte) {
	var f interface{}
	err := json.Unmarshal(data, &f)
	logIfError(err)

	// https://golang.org/doc/effective_go.html#interface_conversions
	m := f.(map[string]interface{})
	fmt.Println("\ngeneric json parsing")
	for k, v := range m {
		switch vv := v.(type) {
		case string:
			fmt.Println("  ", k, "is string:", vv)
		case int:
			fmt.Println("  ", k, "is int:", vv)
		case bool:
			fmt.Println("  ", k, "is bool:", vv)
		case []interface{}:
			fmt.Println("  ", k, "is an array:")
			for _, u := range vv {
				fmt.Println("    ", u)
			}
		default:
			fmt.Println("  ", k, "is of a type I don't know how to handle")
			fmt.Printf("  but I could have checked another way and found %v is a %T\n", v, v)
			// JSONNumber https://golang.org/pkg/encoding/json/#Decoder.UseNumber
			// http://json.org/ no floats so hinting is appreciated
		}
	}
}

// Assuming top level keys are strings, i.e. NOT [] , https://gobyexample.com/json
func rootStringsOnlyParsing(data []byte) map[string]interface{} {
	// A map of string to any type https://blog.golang.org/laws-of-reflection , http://research.swtch.com/interfaces
	var datmap map[string]interface{}
	e := json.Unmarshal(data, &datmap)
	logIfError(e)
	fmt.Println("\nKeys are Strings in a Map:", datmap)
	return datmap
}

// ExampleSimpleObject must be exported to parse correctly , the fields order here is used by json.Marshal output
type ExampleSimpleObject struct {
	Age  int    `json:"age"`
	Name string `json:"name"`
}

// ExampleComplexObject is the magic of auto parsing, if your data never gets corrupted...
// helpful understanding of Go and JSON nesting https://eager.io/blog/go-and-json/
// hints are very powerful leveraging of Reflection that Go core libraries use for JSON
type ExampleComplexObject struct {
	ArrayOfObjects []ExampleSimpleObject `json:"jsonArrayOfObjects,omitempty"`
	ArrayOfStrings []string              `json:"jsonArrayOfStrings"`
	JSONBoolean    bool                  `json:"jsonBoolean"`
	JSONNumber     int                   `json:"jsonNumber"`
	JSONString     string                `json:"jsonString, omitempty"`
	// jsonArrayOfNumbers is not defined and so is not included in the parsed object
}

// autoUnmarshal shows Go structs making parsing JSON look easy https://golang.org/pkg/encoding/json/#example_Unmarshal
func autoUnmarshal(data []byte) ExampleComplexObject {
	var ex ExampleComplexObject
	err := json.Unmarshal(data, &ex)
	logIfError(err)
	fmt.Printf("Auto Unmarshal: %+v \n", ex)
	return ex
}

// writeJSONFile demonstrates the power of interfaces for shared functionality
func writeJSONFile(name string, thing interface{}) {
	theJSON, err := json.MarshalIndent(thing, "", "  ")
	logIfError(err)
	err = ioutil.WriteFile(name, theJSON, 0644)
	logIfError(err)
	// See the omitted fields with: diff --ignore-all-space types.json output.json
}

func main() {
	// https://golang.org/pkg/os/
	myFile, ferr := os.Open("types.json")
	logIfError(ferr)
	readFile(myFile)

	// hint: read a file and return a slice of bytes: https://golang.org/pkg/io/ioutil/#ReadFile
	data, _ := ioutil.ReadFile("types.json")

	genericParsing(data)

	datamap := rootStringsOnlyParsing(data)
	// modifying or adding to a JSON file can be tricky
	datamap["injectedKey"] = "injected value"
	writeJSONFile("dataMapModified.json", datamap)

	auto := autoUnmarshal(data)
	writeJSONFile("autoUnmarshalOmits.json", auto)

	fmt.Println("done")
}
